import React, {useState} from 'react';
import logo from './images/logo.svg';
import './App.css';

function MonApp() {
    const [counter, setCounter] = useState(0)

    function changeCount(event) {
        setCounter(parseInt(event.target.value))
    }

    function displayList() {
     const divs = []
        for (let i = 0; i < counter; i++){
            divs.push(<div className="square" key={i}></div>)
        }
        return divs
        // return new Array(counter).fill().map(()=><div className="square"></div>)
    }



    return (
        <div className="App">
            <div>
                <input type="number" value={counter} onChange={changeCount}/>
            </div>
            <div>
                {displayList()}
            </div>
        </div>
    )
}

export default MonApp;
